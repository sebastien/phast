.TH PHYLOFIT "1" "May 2016" "phyloFit 1.4" "User Commands"
.SH NAME
phyloFit \- Fits one or more tree models to a multiple alignment of DNA
.SH DESCRIPTION
Fits one or more tree models to a multiple alignment of DNA
sequences by maximum likelihood, using the specified tree topology
and substitution model.  If categories of sites are defined via
\fB\-\-features\fR and \fB\-\-catmap\fR (see below), then a separate model will be
estimated for each category.  A description of each model will
be written to a separate file, with the suffix ".mod".  These
\&.mod files minimally include a substitution rate matrix, a tree with
branch lengths, and estimates of nucleotide equilibrium
frequencies.  They may also include information about parameters
for modeling rate variation.
.SH SYNOPSIS
.B phyloFit
[OPTIONS] <msa_fname>
.PP
<msa_fname> should be a multiple alignment in FASTA format or
one of several alternative formats (see \fB\-\-msa\-format\fR).  For
backward compatibility, this argument may be preceded by '\-m' or
\&'\-\-msa'.  Note that \fB\-\-tree\fR is required in most cases.  By default,
all output files will have the prefix "phyloFit" (see
\fB\-\-out\-root\fR).
.SH EXAMPLE
.PP
(If you're like me, you want some basic examples first, and a list
of all options later.)
.PP
1. Compute the distance between two aligned sequences (in FASTA file
pair.fa) under the REV model.
.IP
phyloFit pair.fa
.PP
(output is to phyloFit.mod; distance in substitutions per site
appears in the TREE line in the output file)
.PP
2. Fit a phylogenetic model to an alignment of human, chimp, mouse,
and rat sequences.  Use the HKY85 substitution model.  Write output
to files with prefix "myfile".
.IP
phyloFit \fB\-\-tree\fR "((human,chimp),(mouse,rat))" \fB\-\-subst\-mod\fR HKY85
\fB\-\-out\-root\fR myfile primate\-rodent.fa
.PP
3. As above, but use the discrete\-gamma model for rate variation,
with 4 rate categories.
.IP
phyloFit \fB\-\-tree\fR "((human,chimp),(mouse,rat))" \fB\-\-subst\-mod\fR HKY85
\fB\-\-out\-root\fR myfile \fB\-\-nrates\fR 4 primate\-rodent.fa
.PP
4. As above, but use genome\-wide data, stored in the compact
"sufficient\-statistics" format (can be produced with "msa_view
\fB\-o\fR SS").
.IP
phyloFit \fB\-\-tree\fR "((human,chimp),(mouse,rat))" \fB\-\-subst\-mod\fR HKY85
\fB\-\-out\-root\fR myfile \fB\-\-nrates\fR 4 \fB\-\-msa\-format\fR SS
primate\-rodent.ss
.PP
5. Fit a context\-dependent phylogenetic model (U2S) to an
alignment of human, mouse, and rat sequences.  Use
an EM algorithm for parameter optimization and relax the
convergence criteria a bit (recommended with context\-dependent
models).  Write a log file for the optimization procedure.
Consider only non\-overlapping pairs of sites.
.IP
phyloFit \fB\-\-tree\fR "(human,(mouse,rat))" \fB\-\-subst\-mod\fR U2S \fB\-\-EM\fR
\fB\-\-precision\fR MED \fB\-\-non\-overlapping\fR \fB\-\-log\fR u2s.log \fB\-\-out\-root\fR
hmr\-u2s hmr.fa
.PP
6. As above, but allow overlapping pairs of sites, and compute
likelihoods by assuming Markov\-dependence of columns (see Siepel &
Haussler, 2004).  The EM algorithm can no longer be used
(optimization will be much slower).
.IP
phyloFit \fB\-\-tree\fR "(human,(mouse,rat))" \fB\-\-subst\-mod\fR U2S
\fB\-\-precision\fR MED \fB\-\-log\fR u2s\-markov.log \fB\-\-markov\fR hmr.fa
.PP
7. Compute a likelihood using parameter estimates obtained in (5)
and an assumption of Markov dependence.  This provides a lower
bound on the likelihood of the Markov\-dependent model.
.IP
phyloFit \fB\-\-init\-model\fR hmr\-u2s.mod \fB\-\-lnl\fR \fB\-\-markov\fR hmr.fa
.PP
8. Given an alignment of several mammalian sequences (mammals.fa), a
tree topology (tree.nh), and a set of gene annotations in GFF
(genes.gff), fit separate models to sites in 1st, 2nd, and 3rd
codon positions.  Use the REV substitution model.  Assume coding
regions have feature type 'CDS'.
.IP
phyloFit \fB\-\-tree\fR tree.nh \fB\-\-features\fR genes.gff \fB\-\-out\-root\fR mammals\-rev
\fB\-\-catmap\fR "NCATS = 3; CDS 1\-3" \fB\-\-do\-cats\fR 1,2,3 mammals.fa
.PP
(output will be to mammals\-rev.cds\-1.mod, mammals\-rev.cds\-2.mod, and
mammals\-rev.cds\-3.mod)
.SH OPTIONS
.HP
\fB\-\-tree\fR, \fB\-t\fR <tree_fname>|<tree_string>
.IP
(Required if more than three species, or more than two species
and a non\-reversible substitution model, e.g., UNREST, U2, U3)
Name of file or literal string defining tree topology.  Tree
must be in Newick format, with the label at each leaf equal to
the index or name of the corresponding sequence in the alignment
(indexing begins with 1).  Examples: \fB\-\-tree\fR "(1,(2,3))",
\fB\-\-tree\fR "(human,(mouse,rat))".  Currently, the topology must be
rooted.  When a reversible substitution model is used, the root
is ignored during the optimization procedure.
.HP
\fB\-\-subst\-mod\fR, \fB\-s\fR JC69|F81|HKY85|HKY85+Gap|REV|SSREV|UNREST|R2|R2S|U2|U2S|R3|R3S|U3|U3S
.TP
(default REV).
Nucleotide substitution model.  JC69, F81, HKY85
.IP
REV, and UNREST have the usual meanings (see, e.g., Yang,
Goldman, and Friday, 1994).  SSREV is a strand\-symmetric version
of REV.  HKY85+Gap is an adaptation of HKY that treats gaps as a
fifth character (courtesy of James Taylor).  The others, all
considered "context\-dependent", are as defined in Siepel and
Haussler, 2004.  The options \fB\-\-EM\fR and \fB\-\-precision\fR MED are
recommended with context\-dependent models (see below).
.HP
\fB\-\-msa\-format\fR, \fB\-i\fR FASTA|PHYLIP|MPM|MAF|SS
(default is to guess format from file contents) Alignment format.
FASTA is as usual.  PHYLIP is compatible with the formats used in
the PHYLIP and PAML packages.  MPM is the format used by the
MultiPipMaker aligner and some other of Webb Miller's older tools.
MAF ("Multiple Alignment Format") is used by MULTIZ/TBA and the
UCSC Genome Browser.  SS is a simple format describing the
sufficient statistics for phylogenetic inference (distinct columns
or tuple of columns and their counts).  Note that the program
"msa_view" can be used for file conversion.
.HP
\fB\-\-out\-root\fR, \fB\-o\fR <output_fname_root>
(default "phyloFit").
Use specified string as root filename
for all files created.
.HP
\fB\-\-min\-informative\fR, \fB\-I\fR <ninf_sites>
.IP
Require at least <ninf_sites> "informative" sites \fB\-\-\fR i.e.,
sites at which at least two non\-gap and non\-missing\-data ('N'
or '*') characters are present.  Default is 50.
.HP
\fB\-\-gaps\-as\-bases\fR, \fB\-G\fR
Treat alignment gap characters ('\-') like ordinary bases.
By
.IP
default, they are treated as missing data.
.HP
\fB\-\-ignore\-branches\fR, \fB\-b\fR <branches>
.IP
Ignore specified branches in likelihood computations and parameter
estimation, and treat the induced subtrees as independent.  Can be
useful for likelihood ratio tests.  The argument <branches> should
be a comma\-separated list of nodes in the tree, indicating the
branches above these nodes, e.g., human\-chimp,cow\-dog.  (See
tree_doctor \fB\-\-name\-ancestors\fR regarding names for ancestral nodes.)
This option does not currently work with \fB\-\-EM\fR.
.HP
\fB\-\-quiet\fR, \fB\-q\fR
Proceed quietly.
.HP
\fB\-\-help\fR, \fB\-h\fR
.IP
Print this help message.
(Options for controlling and monitoring the optimization procedure)
.HP
\fB\-\-lnl\fR, \fB\-L\fR
.IP
(for use with \fB\-\-init\-model\fR) Simply evaluate the log likelihood of
the specified tree model, without performing any further
optimization.  Can be used with \fB\-\-post\-probs\fR, \fB\-\-expected\-subs\fR, and
\fB\-\-expected\-total\-subs\fR.
.HP
\fB\-\-EM\fR, \fB\-E\fR
.IP
Fit model(s) using EM rather than the BFGS quasi\-Newton
algorithm (the default).
.HP
\fB\-\-precision\fR, \fB\-p\fR HIGH|MED|LOW
.IP
(default HIGH) Level of precision to use in estimating model
parameters.  Affects convergence criteria for iterative
algorithms: higher precision means more iterations and longer
execution time.
.HP
\fB\-\-log\fR, \fB\-l\fR <log_fname>
.IP
Write log to <log_fname> describing details of the optimization
procedure.
.HP
\fB\-\-init\-model\fR, \fB\-M\fR <mod_fname>
Initialize with specified tree model.
By choosing good
.IP
starting values for parameters, it is possible to reduce
execution time dramatically.  If this option is chosen, \fB\-\-tree\fR
is not allowed.  The substitution model used in the given
model will be used unless \fB\-\-subst\-mod\fR is also specified.
Note: currently only one mod_fname may be specified; it will be
used for all categories.
.HP
\fB\-\-init\-random\fR, \fB\-r\fR
Initialize parameters randomly.
Can be used multiple times to test
.IP
whether the m.l.e. is real.
.HP
\fB\-\-seed\fR, \fB\-D\fR <seed>
.IP
Provide a random number seed for choosing initial parameter values
(usually with \fB\-\-init\-random\fR, though random values are used in some
other cases as well).  Should be an integer >=1.  If not provided,
seed is chosen based on current time.
.HP
\fB\-\-init\-parsimony\fR, \fB\-y\fR
Initialize branch lengths using parsimony counts for given data.
Only currently implemented for models with single character state
(ie, not di\- or tri\-nucleotides).  Other \fB\-\-init\fR options such
as \fB\-\-init\-random\fR or \fB\-\-init\-model\fR can be used in conjunction to
initialize substitution matrix parameters.
.HP
\fB\-\-print\-parsimony\fR, \fB\-Y\fR <filename>
.TP
Print parsimony score to given file, and quit.
(Does not optimize
.IP
or report likelihoods).
.HP
\fB\-\-clock\fR, \fB\-z\fR
Assume a molecular clock in estimation.
Causes the distances to all
descendant leaves to be equal for each ancestral node and cuts the
number of free branch\-length parameters roughly in half.
.HP
\fB\-\-scale\-only\fR, \fB\-B\fR
.IP
(for use with \fB\-\-init\-model\fR) Estimate only the scale of the tree,
rather than individual branch lengths (branch proportions fixed).
Equilibrium frequencies and rate\-matrix parameters will still be
estimated unless \fB\-\-no\-freqs\fR and \fB\-\-no\-rates\fR are used.
.HP
\fB\-\-scale\-subtree\fR, \fB\-S\fR <node_name>
(for use with \fB\-\-scale\-only\fR) Estimate separate scale factors for
subtree beneath identified node and rest of tree.  The branch
leading to the subtree is included with the subtree.  If ":loss" or
":gain" is appended to <node_name>, subtree scale is constrained to
be greater than or less than (respectively) scale for rest of tree.
.HP
\fB\-\-estimate\-freqs\fR, \fB\-F\fR
.IP
Estimate equilibrium frequencies by maximum likelihood, rather
than approximating them by the relative frequencies in the data.
If using the SSREV model, this option implies \fB\-\-sym\-freqs\fR.
.HP
\fB\-\-sym\-freqs\fR, \fB\-W\fR
.IP
Estimate equilibrium frequencies, assuming freq(A)=freq(T) and
freq(C)=freq(G).  This only works for an alphabet ACGT (and possibly
gap).  This option implies \fB\-\-estimate\-freqs\fR.
.HP
\fB\-\-no\-freqs\fR, \fB\-f\fR
.IP
(for use with \fB\-\-init\-model\fR) Do not estimate equilibrium
frequencies; just use the ones from the given tree model.
.HP
\fB\-\-no\-rates\fR, \fB\-n\fR
.IP
(for use with \fB\-\-init\-model\fR) Do not estimate rate\-matrix
parameters; just use the ones from the given tree model.
.HP
\fB\-\-ancestor\fR, \fB\-A\fR <seqname>
Treat specified sequence as the root of the tree.
The tree
topology must define this sequence to be a child of the root
(in practice, the branch from the root to the specified
sequence will be retained, but will be constrained to have
length zero).
.HP
\fB\-\-error\fR, \fB\-e\fR <filename>
.IP
For each parameter, report estimate, variance, and 95%% confidence
interval, printed to given filename, one parameter per line.
.HP
\fB\-\-no\-opt\fR, \fB\-O\fR <param_list>
Hold parameters listed in comma\-separated param_list constant at
initial values.  This applies only to the "main" model, and not to
any models defined with the \fB\-\-alt\-mod\fR option.  Param list can
contain values such as "branches" to hold branch lengths constant,
"ratematrix", "backgd", or "ratevar" to hold entire rate matrix,
equilibrium frequencies, or rate variation parameters constant
(respectively).  There are also substitution model\-specific
parameters such as "kappa" (transition/transversion rate ratio).
Note: to hold certain branches constant, but optimize others,
put an exclamation point in the newick\-formatted tree after the
branch lengths that should be held constant.  This can be useful
for enforcing a star\-phylogeny.  However, note that the two branches
coming from root of tree are treated as one.  So they should both
be held constant, or not held constant.  This option does *not* work
with \fB\-\-scale\-only\fR or \fB\-\-clock\fR.
.HP
\fB\-\-bound\fR <param_name[lower_bound,upper_bound]>
Set boundaries for parameter.
lower_bound or upper_bound may be
empty string to keep default.
For example \fB\-\-bound\fR gc_param[1,] will
.IP
set the lower bound for gc_param to 1 (keeping upper bound at infinity),
for a GC model.  Only applies to parameters for model in the "main"
tree, but similar syntax can be used within the \fB\-\-alt\-mod\fR arguments.
Can be used multiple times to set boundaries for different parameters.
.HP
\fB\-\-selection\fR <selection_param>
.IP
Use selection in the model (is also implied if \fB\-\-init\-model\fR is used
and contains selection parameter).  Selection scales rate matrix
entries by selection_param/(1\-exp(\fB\-selection\-param\fR)); this is done
after rate matrix is scaled to set expected number of substitutions
per unit time to 1.  If using codon models selection acts only on
nonysynonymous mutations.
(Options for modeling rate variation)
.HP
\fB\-\-nrates\fR, \fB\-k\fR <nratecats>
(default 1).
Number of rate categories to use.  Specifying a
.IP
value of greater than one causes the discrete gamma model for
rate variation to be used (Yang, 1994).
.HP
\fB\-\-alpha\fR, \fB\-a\fR <alpha>
(for use with \fB\-\-nrates\fR).
Initial value for alpha, the shape
parameter of the gamma distribution.
Default is 1.
.HP
\fB\-\-rate\-constants\fR, \fB\-K\fR <rate_consts>
.IP
Use a non\-parameteric mixture model for rates, instead of
assuming a gamma distribution.  The argument <rate_consts>
must be a comma\-delimited list explicitly defining the rate
constants to be used.  The "weight" (mixing proportion)
associated with each rate constant will be estimated by EM
(this option implies \fB\-\-EM\fR).  If \fB\-\-alpha\fR is used with
this option, then the mixing proportions will be initialized
to reflect a gamma distribution with the specified shape
parameter.
(Options for separate handling of sites in different annotation categories)
.HP
\fB\-\-features\fR, \fB\-g\fR <fname>
.IP
Annotations file (GFF or BED format) describing features on
one or more sequences in the alignment.  Together with a
category map (see \fB\-\-catmap\fR), will be taken to define site
categories, and a separate model will be estimated for each
category.  If no category map is specified, a category will be
assumed for each type of feature, and they will be numbered in
the order of appearance of the features.  Features are assumed
to use the coordinate frame of the first sequence in the
alignment and should be non\-overlapping (see 'refeature
\fB\-\-unique\fR').
.HP
\fB\-\-catmap\fR, \fB\-c\fR <fname>|<string>
.IP
(optionally use with \fB\-\-features\fR) Mapping of feature types to
category numbers.  Can either give a filename or an "inline"
description of a simple category map, e.g., \fB\-\-catmap\fR "NCATS =
3 ; CDS 1\-3" or \fB\-\-catmap\fR "NCATS = 1 ; UTR 1".  Note that
category 0 is reserved for "background" (everything that is
not described by a defined feature type).
.HP
\fB\-\-do\-cats\fR, \fB\-C\fR <cat_list>
.IP
(optionally use with \fB\-\-features\fR) Estimate models for only the
specified categories (comma\-delimited list categories, by name
or numbera).  Default is to fit a model for every category.
.HP
\fB\-\-reverse\-groups\fR, \fB\-R\fR <tag>
.IP
(optionally use with \fB\-\-features\fR) Group features by <tag> (e.g.,
"transcript_id" or "exon_id") and reverse complement
segments of the alignment corresponding to groups on the
reverse strand.  Groups must be non\-overlapping (see refeature
\fB\-\-unique\fR).  Useful with categories corresponding to
strand\-specific phenomena (e.g., codon positions).
(Options for context\-dependent substitution models)
.HP
\fB\-\-markov\fR, \fB\-N\fR
.IP
(for use with context\-dependent substitutions models and not
available with \fB\-\-EM\fR.)  Assume Markov dependence of alignment
columns, and compute the conditional probability of each
column given its N\-1 predecessors using the two\-pass algorithm
described by Siepel and Haussler (2004).  (Here, N is the
"order" of the model, as defined by \fB\-\-subst\-mod\fR; e.g., N=1
for REV, N=2 for U2S, N=3 for U3S.) The alternative (the
default) is simply to work with joint probabilities of tuples
of columns.  (You can ensure that these tuples are
non\-overlapping with the \fB\-\-non\-overlapping\fR option.)  The use
of joint probabilities during parameter estimation allows the
use of the \fB\-\-EM\fR option and can be much faster; in addition, it
appears to produce nearly equivalent estimates.  If desired,
parameters can be estimated without \fB\-\-markov\fR, and
then the likelihood can be evaluated using \fB\-\-lnl\fR and
\fB\-\-markov\fR together.  This gives a lower bound on the
likelihood of the Markov\-dependent model.
.HP
\fB\-\-non\-overlapping\fR, \fB\-V\fR
(for use with context\-dependent substitution models; not
compatible with \fB\-\-markov\fR, \fB\-\-features\fR, or
\fB\-\-msa\-format\fR SS) Avoid using overlapping tuples of sites
in parameter estimation.  If a dinucleotide model is selected,
every other tuple will be considered, and if a nucleotide
triplet model is selected, every third tuple will be
considered.  This option cannot be used with an alignment
represented only by unordered sufficient statistics.
(Option for lineage\-specific models)
.HP
\fB\-\-label\-branches\fR branch1,branch2,branch3...:label
.IP
Create a group of branches by giving a set of branches a
single label.  The label should be a word which does not
contain special characters (in particular, no spaces, brackets,
parentheses, pound signs, commas, or colons).
The label is for use with \fB\-\-alt\-model\fR option below, so that an
alternate model can be defined for a set of branches.  A branch
is specified by the name of the node which is a descendant of
that branch.
.IP
For example,
\fB\-\-label\-branches\fR hg18,chimp,hg18\-chimp:HC
will apply the label "HC" to the hg18,chimp,and hg18\-chimp
branches in the following tree:
(((hg18,chimp)hg18\-chimp, (mouse,rat)mouse\-rat)
.IP
The same label could be defined without using \fB\-\-label\-branches\fR
by specifying the tree (either on the command\-line or within
init\-model) as follows:
(((hg18 # HC, chimp #HC)#HC, (mouse,rat))
.HP
\fB\-\-label\-subtree\fR node[+]:label
Similar to label\-branches, except labels the entire subtree
of the named node.  If the node name is followed by a "+" sign,
then includes the branch leading up to the node in the subtree.
.HP
\fB\-\-alt\-model\fR, \fB\-d\fR <label:(model|param_list)>
Create a lineage\-specific substitution model on a group of branches.
The group is defined by a label, which can be specified within
the tree string (using the # sign notation), or by using the
\fB\-\-label\-branches\fR or \fB\-\-label\-subtree\fR arguments.  If the alt\-model
applies to only a single branch, labelling is not necessary and
the name of the node descending from the branch can be used instead.
See \fB\-\-label\-branches\fR above for more details on labelling groups of
branches.
If a name of a substitution model (HKY85, REV, UNREST, etc)
is given after the colon, then this model will be used for the
group of branches, and parameters relevant to the model will be
estimated separately in this group.  This model may be different
(or the same) as the model used in the rest of the tree, but it
must have the same number of states and be of the same order as
the model used for the rest of the tree.
.IP
Alternately, a list of parameter names can be given after the colon.
In this case, the same substitution model will be used for the
entire tree, but the parameters listed will be estimated separately
in the specified group of branches.
.IP
The parameter names are model\-specific, and include "kappa" for
HKY models, "alpha" for GC models, "ratematrix" to specify
all rate\-matrix parameters in general models, and "backgd" for
the equilibrium background frequencies.  The parameter names
may optionally be followed by boundaries in square brackets to
specify parameter bounds, as described in \fB\-\-bound\fR option.
.IP
The \fB\-\-alt\-model\fR argument may be used multiple times, if one
wishes to (for example) optimize a parameter independently
on several different groups of branches.
Example:
phyloFit align.fa \fB\-\-subst\-mod\fR HKY85 \e
\fB\-\-tree\fR "(human, (mouse#MR, rat#MR)#MR, cow)"\e
\fB\-\-alt\-model\fR "MR:kappa[0, 1]"
.IP
will estimate the HKY85 parameter kappa separately on the
mouse/rat subtree, and constrain kappa between 0 and 1.  The
quotes are often necessary to prevent the square brakcets from
being parsed by the shell.  The same model could be achieved with:
.IP
phyloFit align.fa \fB\-\-subst\-mod\fR HKY85 \e
\fB\-\-tree\fR "(human, (mouse,rat)mouse\-rat, cow)"\e
\fB\-\-label\-branches\fR mouse,rat,mouse\-rat:MR \e
\fB\-\-alt\-model\fR "MR:kappa[0,1]"
or
.IP
phyloFit align.fa \fB\-\-subst\-mod\fR HKY85 \e
\fB\-\-tree\fR "(human, (mouse,rat)mouse\-rat, cow)" \e
\fB\-\-label\-subtree\fR "mouse\-rat+:MR" \e
\fB\-\-alt\-model\fR "MR:kappa[0,1]"
.IP
(Options for posterior probabilities)
.HP
\fB\-\-post\-probs\fR, \fB\-P\fR
.IP
Output posterior probabilities of all bases at all ancestral
nodes.  Output will be to auxiliary file(s) with suffix
".postprobs".
.HP
\fB\-\-expected\-subs\fR, \fB\-X\fR
.IP
Output posterior expected number of substitutions on each branch at
each site, summed across all types of substitutions.
Output will be to auxiliary file(s) with suffix ".expsub".
.HP
\fB\-\-expected\-subs\-col\fR, \fB\-J\fR
.IP
Output posterior expected number of substitutions of each type
on each branch, for each site.  Output will be to auxiliary
file(s) with suffix .expcolsub
.HP
\fB\-\-expected\-total\-subs\fR, \fB\-Z\fR
.IP
Output posterior expected number of substitutions of each type
on each branch, summed across all sites.  Output will be to
auxiliary file(s) with suffix ".exptotsub".
.HP
\fB\-\-column\-probs\fR, \fB\-U\fR
.TP
(for use with \fB\-init\-model\fR; implies \fB\-\-lnl\fR)
Output a separate log
.TP
probability for each type of column in the input.
Output will
.TP
be to a file with suffix ".colprobs".
Values are log base 2.
.IP
(Options for estimation in sliding window)
.HP
\fB\-\-windows\fR, \fB\-w\fR <size,shift>
Apply a sliding window to the alignment, and fit a separate
tree to each window.  Arguments specify size of window and
amount by which to shift it on each iteration, both in bases
of the first sequence in the alignment (assumed to be the
reference sequence).  Separate versions of all output files
will be created for each window.
.HP
\fB\-\-windows\-explicit\fR, \fB\-v\fR <window_coord_list>
Like \fB\-\-windows\fR, except that all start and end coordinates must
be explicitly specified.  Each successive pair of numbers is
interpreted as defining the start and end of a window.  Can be
used with a two\-column file and the '*' operator, e.g.,
\fB\-\-windows\-explicit\fR '*mycoords'.
.SH SEE ALSO
.TP
A. Siepel and D. Haussler.
2004.  Phylogenetic estimation of
context\-dependent substitution rates by maximum likelihood.
Mol. Biol. Evol., 21:468\-488.
.TP
Z. Yang, N. Goldman, and A. Friday.
1994. Comparison of models for
nucleotide substution used in maximum likelihood phylogenetic
estimation. Mol. Biol. Evol., 11:316\-324.
.TP
Z. Yang. 1994. Maximum likelihood phylogenetic estimation from
DNA sequences with variable rates over sites: approximate
methods. J. Mol. Evol., 39:306\-314.
